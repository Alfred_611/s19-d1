// Exponent Operator

const firstNum = 8 ** 2
console.log(firstNum);

// Exponent ES6 Updates
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals

let name = "John";

// Pre-template literal string
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals: " + message);

// Using Template Literal (Backticks - `` and $ signs)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

const anotherMessage = `
${name} attended a "math competition".
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}
`;

console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// Array and Object Destructuring

// Array destructuring - allows to unpack elements in arrays into distinct values

const fullName = ["Juan", "Peter", "Jane"];

// Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]}, ${fullName[1]}, ${fullName[2]}! It's nice to meet you.`);

// Array destructuring
const [firstKoi, middleKoi, lastKoi] = fullName;
console.log(`Hello ${firstKoi}, ${middleKoi}, ${lastKoi}! It's nice to meet you.`);





// Object Destructuring - allows us to unpack properties of objects into distinct variables.

const person = {
	givenName: "Camille",
	maidenName: "Serrano",
	familyName: "Manalang"
}

// Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring
const {givenName, maidenName, familyName} = person; 
console.log(givenName); // once destructured na pwede na tanggalin si person kasi initialized na nung dinestructure sa taas
console.log(maidenName);
console.log(familyName);
function getFullName ({givenName, maidenName, familyName}){ //naka enclose parin sa curly brackets kasi destructured properties sila
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

/* Arrow functions - compact alternative syntax to traditional functions
   Conventional functions
	function nameFunction(){
	statements/conditions to be executed;
	}

	- Arrow functions Syntax:
		const variable = () => 
			console.log();
		}
*/
/*
function printFullName(firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D.", "Smith");
*/

const printFullName = (givenName, middleName, familyName) => {
	console.log(`${givenName} ${middleName} ${familyName}`);
}

printFullName("John", "D.", "Smith");

const students = ["John", "Jane", "Judy"];

console.log(`Pre-Arrow Function`)
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

console.log(`Arrow Function`)
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

/* MATH STUFF NAMAN
	const add = (x, y) => {
		return x + y;
	}
*/

const add = (x, y) => x + y; //omit na curly brackets and keyword

console.log(add(5, 8));

let total = add(1, 6);
console.log(total);

// Default Function Argument Value - MAGAADD NG DEFAULT ARGUMENT VALUE PAG WALANG NILAGAY NA ARGUMENT

const greet = (name = 'User') => {
	return `Good morning, ${name}`;
}

console.log(greet());
console.log(greet("Peter")); //-- maooverride na kasi may Peter na

// Create a default function argument value for setting a default pokemon
// I got you ${pokemon}

// Class-based object Blueprints - allows creation/instantiation of objects using classes as blueprints
/* Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objecetPropertyB;
		}
	}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);












const opener = (pokemon = 'Rayquaza') => {
	return `I got you, ${pokemon}!`;
}

console.log(opener());